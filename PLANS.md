## GOALS For The Project
- Allow Drive Encryption with lvm
- Allow Users to install their favourite DE or WM Right From The Script
- Make A tutor mode where the user can learn about every command that is being executed.
- Make A Post Install script which can do other stuff like install software set locales, change timezones etc. just like CloverOS