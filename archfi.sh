#! /bin/bash
echo "Hi"
echo "Welcome To The Installer" 
lsblk
read -p "Choose The Disk You Want To Install Arch To (eg. /dev/sda) " IDISK
echo "You Have Chosen $IDISK"
echo "Click CTRL + c to exit the script. This May result in an unusable setup"
echo "Now The Installer is going to ask you about few basic things."
read -p "What is the timezone you want to use eg. Asia/Kolkata for IST : " TIMEZONEUSER
read -p "What Hostname Do You Want To use : " HOSTNAMEOFNEWINSTALL
read -p "What Is Your ROOT PASSWORD. Note: Passwords entered during the install will be visible :) : " ROOTPASSWD
read -p "what is The Username Of The User : " USERSNAMEY
read -p "What is the password of the normal user : " USERSPASSWDLOL
read -p "What Linux Kernel Do You Want to use (options are linux linux-zen linux-lts)" KERNEL
echo "DE, WM related stuff will be asked after the install finishes along with aur helper related stuff"
echo "Thank You For Giving The Required Information" 
FILE="/sys/firmware/efi/efivars"
if [ -e "$FILE" ]
then
  echo "You are Using EFI"
  echo "Make Sure You Dont Have $IDISK mounted anywhere"
  parted -a optimal $IDISK --script mklabel gpt
            parted $IDISK --script mkpart primary 1MiB 513MiB
            parted $IDISK --script -- mkpart primary 513MiB -1
  partprobe
  part_1=("${IDISK}1")
  part_2=("${IDISK}2")
  part_3=("${IDISK}3")
  headers=("${KERNEL}-headers") 
  mkfs.fat -I -F 32 $part_1
  mkfs.ext4 -F $part_2
  mkdir /mnt
  mount $part_2 /mnt
  mkdir /mnt/boot
  mount $part_1 /mnt/boot
  pacstrap /mnt base base-devel $KERNEL linux-firmware $headers
  genfstab -U /mnt >> /mnt/etc/fstab
  arch-chroot /mnt ln -sf /usr/share/zoneinfo/$TIMEZONEUSER /etc/localtime
  arch-chroot /mnt hwclock --systohc
  rm -rf /mnt/etc/locale.gen
  touch /mnt/etc/locale.gen
  echo "en_UTF-8 UTF-8" >> /mnt/etc/locale.gen
  arch-chroot /mnt locale-gen
  touch /mnt/etc/locale.conf
  echo "LANG=en_US.UTF-8" >> /mnt/etc/locale.conf
  arch-chroot /mnt export LANG=en_US.UTF-8
  echo $HOSTNAMEOFNEWINSTALL >> /mnt/etc/hostname
  echo "127.0.0.1  localhost" >> /mnt/etc/hosts
  echo "::1        localhost" >> /mnt/etc/hosts
  echo "127.0.1.1  $HOSTNAMEOFNEWINSTALL" >> /mnt/etc/hosts
  arch-chroot /mnt echo -e "$ROOTPASSWD\n$ROOTPASSWD" | passwd root
  arch-chroot /mnt pacman --noconfirm -S grub efibootmgr os-prober networkmanager
  arch-chroot /mnt grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot
  arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
  arch-chroot /mnt useradd -m $USERNAMEY
  arch-chroot /mnt usermod -aG wheel $USERNAMEY
  arch-chroot /mnt echo -e "$USERPASSWDLOL\n$USERPASSWDLOL" | passwd $USERNAMEY
  arch-chroot /mnt systemctl enable NetworkManager.service
  else
  echo "You are Using Legacy BIOS"
  parted -a optimal $IDISK --script mklabel msdos
  parted $IDISK --script -- mkpart primary 1MiB -1
  partprobe
  part_1=("${IDISK}1")
  headers=("${KERNEL}-headers") 
  mkfs.ext4 -F $part_1
  mkdir /mnt
  mount $part_1 /mnt
  mkdir /mnt/home
  pacstrap /mnt base base-devel $KERNEL linux-firmware $headers
  genfstab -U /mnt >> /mnt/etc/fstab
  arch-chroot /mnt ln -sf /usr/share/zoneinfo/$TIMEZONEUSER /etc/localtime
  echo "TimeZone Has Been Set As $TIMEZONEUSER"
  rm -rf /mnt/etc/locale.gen
  arch-chroot /mnt touch /etc/locale.gen
  arch-chroot /mnt echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
  arch-chroot /mnt locale-gen
  touch /mnt/etc/locale.conf
  arch-chroot /mnt echo "LANG=en_US.UTF-8" >> /etc/locale.conf
  arch-chroot /mnt export LANG=en_US.UTF-8
  echo $HOSTNAMEOFNEWINSTALL >> /mnt/etc/hostname
  echo "127.0.0.1  localhost" >> /mnt/etc/hosts
  echo "::1        localhost" >> /mnt/etc/hosts
  echo "127.0.1.1  $HOSTNAMEOFNEWINSTALL" >> /mnt/etc/hosts
  arch-chroot /mnt echo -e "$ROOTPASSWD\n$ROOTPASSWD" | passwd root
  arch-chroot /mnt pacman -S grub os-prober networkmanager
  arch-chroot /mnt grub-install $IDISK
  arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
  arch-chroot /mnt useradd -m $USERNAME
  arch-chroot /mnt usermod -aG wheel $USERNAME
  arch-chroot /mnt echo -e "$USERPASSWDLOL\n$USERPASSWDLOL" | passwd USERNAME
  arch-chroot /mnt systemctl enable NetworkManager.service
fi
echo "Install Finished :) "
